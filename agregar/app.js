const app = new Vue({
    el : '#app',
    data:{
        titulo: 'Nuevo Curso de Vuejs por euglis Lopez',
        numeros: [
            {nombre: 'Uno', valor:1},
            {nombre: 'dos', valor:2},
            {nombre: 'Tres', valor:3}
        ],
        nuevoCar: '',
        total: 0 
    },
    methods:{
        agregarCar(){
            this.numeros.push({
                nombre: this.nuevoCar, valor: 0
            })
        }
    },
    computed: {
        sumarCar(){
            this.total= 0;
            for(numero of this.numeros){
                this.total = this.total + numero.valor;
            }
            return this.total;
        }
    }
})