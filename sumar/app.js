const app = new Vue({
    el: '#app',
    data:{
        titulo: 'hola este es mi curso',
        products:[
            {nombre:'perrarina',precio:3.5, cantidad:1},
            {nombre:'harina',precio:2, cantidad:2},
            {nombre:'cocacola',precio:6.3, cantidad:1},
            {nombre:'doritos',precio:5, cantidad:1}
        ],
        total: 0
    },
    computed:{
        totalProducts () {
            this.total = 0;
            for ( product of this.products) {
                this.total += product.cantidad * product.precio;
            }  
            return this.total;
        }
    }
});
